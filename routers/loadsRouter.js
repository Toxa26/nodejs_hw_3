const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('../helpers');

const {
  getLoads,
  addLoad,
  getActiveLoads,
  loadProgress,
  getLoadById,
  updateLoadById,
  deleteLoadById,
  postALoadById,
  getShippingInfo,
} = require('../controllers/loadsController');

router.get('/', asyncWrapper(getLoads));
router.post('/', asyncWrapper(addLoad));
router.get('/active', asyncWrapper(getActiveLoads));
router.patch('/active/state', asyncWrapper(loadProgress));
router.get('/:id', asyncWrapper(getLoadById));
router.put('/:id', asyncWrapper(updateLoadById));
router.delete('/:id', asyncWrapper(deleteLoadById));
router.post('/:id/post', asyncWrapper(postALoadById));
router.get('/:id/shipping_info', asyncWrapper(getShippingInfo));

module.exports = router;
