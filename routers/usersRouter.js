const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('../helpers');

const {
  getUser,
  deleteUser,
  changePassword,
} = require('../controllers/userController');

router.get('/me', asyncWrapper(getUser));
router.delete('/me', asyncWrapper(deleteUser));
router.patch('/me/password', asyncWrapper(changePassword));

module.exports = router;
