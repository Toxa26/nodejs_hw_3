const express = require('express');
const router = express.Router();

const {asyncWrapper} = require('../helpers');
const {
  login,
  register,
  forgotPassword,
} = require('../controllers/authController');
const {
  validateLogin,
  validateRegister,
  validateForgotPassword,
} = require('../middlewares/validationMiddleware');

router.post('/login', asyncWrapper(validateLogin), asyncWrapper(login));
router.post('/register',
    asyncWrapper(validateRegister),
    asyncWrapper(register));
router.post('/forgot_password',
    asyncWrapper(validateForgotPassword),
    asyncWrapper(forgotPassword));


module.exports = router;
