const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('../helpers');

const {
  getTrucks,
  addTruck,
  getTruckById,
  updateTruckById,
  deleteTruckById,
  assignTruckById,
} = require('../controllers/trucksController');

router.get('/', asyncWrapper(getTrucks));
router.post('/', asyncWrapper(addTruck));
router.get('/:id', asyncWrapper(getTruckById));
router.put('/:id', asyncWrapper(updateTruckById));
router.delete('/:id', asyncWrapper(deleteTruckById));
router.post('/:id/assign', asyncWrapper(assignTruckById));

module.exports = router;
