class NoActiveLoad extends Error {
  constructor(message = 'You have no active loads') {
    super(message);
    this.statusCode = 400;
  }
};

module.exports = {
  NoActiveLoad,
};
